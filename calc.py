

class Calculate:
    """Docstring"""

    def add(self, x, y):
        """Docstring"""
        if (isinstance(x, int) or isinstance(x, float)) and (isinstance(y, int) or isinstance(y, float)):
            return x + y
        else:
            raise TypeError("Invalid type:{0} and {1}".format(isinstance(x), isinstance(y)))

    def sub(self, x, y):
        """Docstring"""
        if (isinstance(x, int) or isinstance(x, float)) and (isinstance(y, int) or isinstance(y, float)):
            return x - y
        else:
            raise TypeError("Invalid type:{0} and {1}".format(isinstance(x), isinstance(y)))

    def mul(self, x, y):
        """Docstring"""
        if (isinstance(x, int) or isinstance(x, float)) and (isinstance(y, int) or isinstance(y, float)):
            return x * y
        else:
            raise TypeError("Invalid type:{0} and {1}".format(isinstance(x), isinstance(y)))

    def div(self, x, y):
        """Docstring"""
        if (isinstance(x, int) or isinstance(x, float)) and (isinstance(y, int) or isinstance(y, float)):
            return x / y
        else:
            raise TypeError("Invalid type:{0} and {1}".format(isinstance(x), isinstance(y)))

    def mod(self, x, y):
        """Docstring"""
        if (isinstance(x, int) or isinstance(x, float)) and (isinstance(y, int) or isinstance(y, float)):
            return x % y
        else:
            raise TypeError("Invalid type:{0} and {1}".format(isinstance(x), isinstance(y)))

if __name__ == '__main__':
    calc = Calculate()

    x = 2
    y = 2

    result = calc.add(x, y)
    print("\tAdd: {0} + {1} = {2}".format(x, y, result))

    result = calc.sub(x, y)
    print("\tSub: {0} - {1} = {2}".format(x, y, result))

    result = calc.mul(x, y)
    print("\tMul: {0} * {1} = {2}".format(x, y, result))

    result = calc.div(x, y)
    print("\tDiv: {0} / {1} = {2}".format(x, y, result))

    result = calc.mod(x, y)
    print("\tMod: {0} % {1} = {2}".format(x, y, result))
