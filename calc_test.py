import unittest
from calc import Calculate

class TestCal(unittest.TestCase):
    def setUp(self):
        self.calc = Calculate()


    # Addition tests
    def test_add_correct(self):
        self.assertEqual(4, self.calc.add(2.0, 2))

    def test_add_strings_error(self):
        self.assertRaises(TypeError, self.calc.add, "this", "that")
        

    # Subtraction tests
    def test_sub_correct(self):
        self.assertEqual(0, self.calc.sub(2.0, 2))

    def test_sub_strings_error(self):
        self.assertRaises(TypeError, self.calc.sub, "this", "that")
        

    # Multiplication tests
    def test_mul_correct(self):
        self.assertEqual(4, self.calc.mul(2.0, 2))

    def test_mul_strings_error(self):
        self.assertRaises(TypeError, self.calc.mul, "this", "that")
        

    # Division tests
    def test_div_correct(self):
        self.assertEqual(1, self.calc.div(2.0, 2))

    def test_div_strings_error(self):
        self.assertRaises(TypeError, self.calc.div, "this", "that")
        

    # Modulo tests
    def test_mod_correct(self):
        self.assertEqual(0, self.calc.mod(2.0, 2))

    def test_mod_strings_error(self):
        self.assertRaises(TypeError, self.calc.mod, "this", "that")
        
if __name__=='__main__':
    unittest.main()
